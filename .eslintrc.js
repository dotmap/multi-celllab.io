module.exports = {
  root: true,
  parser: "babel-eslint",
  env: {
    browser: true,
    node: true
  },
  extends: "standard",
  // required to lint *.vue files
  plugins: [
    "html"
  ],
  // add your custom rules here
  rules: {
    "semi": ["error", "always"],
    "quotes": ["error", "double"],
    "indent": ["error", "tab"],
		"no-tabs": 0,
  },
  globals: {}
};
