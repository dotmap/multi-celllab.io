module.exports = {
	/*
	** Headers of the page
	*/
	head: {
		titleTemplate: "%s | multi-cell",
		meta: [
			{ charset: "utf-8" },
			{ name: "viewport", content: "width=device-width, initial-scale=1" },
			{ hid: "description", name: "description", content: "Nuxt.js project for multi-cell's website" }
		],
		link: [
			{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
			{ rel: "stylesheet", href: "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" },
			{ rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Nunito" }
		]
	},
	css: [
		"bulma/bulma.sass"
	],
	/*
  ** Customize the progress-bar color
  */
	loading: { color: "#3B8070" },
	/*
	** Build configuration
	*/
	build: {
		extractCSS: true,
		/*
		** Run ESLINT on save
		*/
		extend (config, ctx) {
			if (ctx.isClient) {
				config.module.rules.push({
					enforce: "pre",
					test: /\.(js|vue)$/,
					loader: "eslint-loader",
					exclude: /(node_modules)/
				});
			}
		}
	}
};
